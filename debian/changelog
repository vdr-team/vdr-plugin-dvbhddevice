vdr-plugin-dvbhddevice (2.2.0-16) unstable; urgency=medium

  * build against vdr 2.6.9
  * add myself to uploaders
  * change build-depends from obsolete pkg-config to pkgconf
  * update standards-version to 4.7.0

 -- Christoph Martin <chrism@debian.org>  Mon, 17 Feb 2025 23:22:48 +0100

vdr-plugin-dvbhddevice (2.2.0-15) unstable; urgency=medium

  * Build-depend on vdr 2.6.0 and update standards version to 4.6.0
  * Make sure CPPFLAGS is used to allow security hardening options to be set

 -- Tobias Grimm <etobi@debian.org>  Wed, 05 Jan 2022 21:43:10 +0100

vdr-plugin-dvbhddevice (2.2.0-14) unstable; urgency=medium

  * Update debhleper-compat to 13
  * Build-depend on vdr >= 2.4.7

 -- Tobias Grimm <etobi@debian.org>  Wed, 15 Dec 2021 08:31:05 +0100

vdr-plugin-dvbhddevice (2.2.0-13) unstable; urgency=medium

  * Standards-Version: 4.4.1

 -- Tobias Grimm <etobi@debian.org>  Fri, 01 Nov 2019 13:32:44 +0100

vdr-plugin-dvbhddevice (2.2.0-12) unstable; urgency=medium

  [ Jelmer Vernooĳ ]
  * Use secure copyright file specification URI.

  [ Tobias Grimm ]
  * Set upstream metadata fields: Name

 -- Tobias Grimm <etobi@debian.org>  Mon, 29 Jul 2019 19:30:09 +0200

vdr-plugin-dvbhddevice (2.2.0-11) unstable; urgency=medium

  * Made pkg-config configurable in Makefile to allow cross-builds
    (Closes: #932932)
  * Standards-Version: 4.4.0

 -- Tobias Grimm <etobi@debian.org>  Sun, 28 Jul 2019 20:01:24 +0200

vdr-plugin-dvbhddevice (2.2.0-10) unstable; urgency=medium

  * Added AUDIO_GET_PTS ioctl, which was dropped from the kernel
  * Build-depend on vdr-dev >= 2.4.1

 -- Tobias Grimm <etobi@debian.org>  Thu, 18 Jul 2019 08:29:50 +0200

vdr-plugin-dvbhddevice (2.2.0-9) unstable; urgency=medium

  * VCS moved to salsa.debian.org
  * Replaced priority extra with optional
  * Build-depend on vdr-dev (>= 2.4.0)

 -- Tobias Grimm <etobi@debian.org>  Sun, 15 Apr 2018 18:59:48 +0200

vdr-plugin-dvbhddevice (2.2.0-8) unstable; urgency=medium

  * Build-depend on vdr-dev >= 2.3.8

 -- Tobias Grimm <etobi@debian.org>  Tue, 15 Aug 2017 21:49:18 +0200

vdr-plugin-dvbhddevice (2.2.0-7) unstable; urgency=low

  * This plugin was shipped with VDR previously and is now a separate package.

 -- Tobias Grimm <etobi@debian.org>  Fri, 06 Jan 2017 21:36:49 +0100
